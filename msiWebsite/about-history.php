<?php
include('header.php');
?>



<div class="container pageContent">
  <img class="d-block w-100" src="images/IMG_8851.png">
  <br>
    <div class="row">
    <?php
    include('sidebar.php');
    ?>
      <!-- /.col-md-4 -->
      <div class="col-md-9">
        <h2 class = "font-weight-light">About Us</h2>
        <p>The Marine Science Institute Library is a specialized library that supports the institute’s objectives of research, graduate level training and extension services. <br><br>
        As a comprehensive source of information in various fields of marine biology, oceanography, ecology, environmental management, fisheries and aquaculture, the library maintains a collection of books and monographs, journals, audio-visual and cartographic materials, vertical files and reprints. These are all searchable through the Online Public Access Catalogue or OPAC.
        <br><br>
        It houses the institute’s significant contribution to tropical marine science since its establishment in 1974.  Publications are kept in the library ranging from ISI journal articles, conference proceedings, book and book chapters, reviews, etc. Copies of publications (PDF), both ISI and non-ISI, of the faculty,  research staff and graduate students, may be requested from the Head Librarian.
        <br><br>
        The library monitors these publications regularly for prompt referral. Services and facilities include: book loans, reference and literature search, facilities for scanning and photocopying, a room for viewing audio-visual materials and Wi-Fi connection for our user’s Internet needs. </p>
      </div>
    <div class="row">
      <div class="col-md-6">
      </div>
      <div class="col-md-6">
      </div>
    </div>
  </div>

<?php
include('footer.php');
?>