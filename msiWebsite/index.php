<?php
include('header.php');
?>

  <!-- <div class="container-fluid pageContent"> -->
  <div class="container pageContent">

    <!-- Heading Row -->
    <div class="row align-items-center my-5">
      <div class="col-lg-7">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="images/IMG_8851.png" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="images/IMG_8851.png" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="images/IMG_8851.png" alt="Third slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <!-- /.col-lg-8 -->
      <div class="col-lg-5">
        <h1 class="font-weight-bold">The Marine Science Institute Library</h1>
        <h3>College of Science</h3>
        <p>University of the Philippines Diliman</p>
        <a class="btn btn-primary" href="#">Contact Us</a>
      </div>
      <!-- /.col-md-4 -->
    </div>

    <div class="row">
		<?php
		include('sidebar.php');
		?>
      <!-- /.col-md-4 -->
      <div class="col-md-9">
        <h2 class = "font-weight-light">Featured Publications</h2>
        <div class = "row">
        <div class="col-md-6">
          <div class="card publications">
            <!-- <img class="card-img-top" src="images/pub1.webp"> -->
            <div class="card-body d-flex flex-column">
              <h5 class="card-title">Santiañez WJE, Wynne MJ. (2018). Evidence for the treatment of  Talarodictyon tilesii as an older taxonomic synonym of Hydroclathrus stephanosorus (scytosiphonaceae, phaeophyceae). Japanese Society of Phycology, 67: 82–85.</h5>
              <p class="card-text">Morphological and anatomical evidence is presented to support the taxonomic judgment that Talarodictyon .tilesii Endlicher is conspecific with stephanosorus Kraft in Kraft & Abbott. Because the former name has nomenclatural priority over the latter name, Hydroclathrus tilesii(Endlicher) comb. nov. is proposed.Hydroclathrus</p>
              <a href="#" class="btn btn-primary mt-auto">Go somewhere</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card publications">
            <!-- <img class="card-img-top" src="images/pub2.webp"> -->
            <div class="card-body d-flex flex-column">
              <h5 class="card-title">Roleda MY, Marfaing H, Desnica N, Jónsdóttir R, Skjermo J, Rebours C, Nitschke U. (2019). Variations in polyphenol and heavy metal contents of wild-harvested and cultivated seaweed bulk biomass: Health risk assessment and implication for food applications. Food Control, 95: 121–134.</h5>
              <p class="card-text">Seaweeds are increasingly used in European cuisines due to their nutritional value. Many algal constituents, such as polyphenols, are important antioxidants and thus considered beneficial to humans. However, many seaweed species can accumulate heavy metals and exhibit potential health risks upon ingestion. We investigated temporal and spatial variations in polyphenol and heavy metal (As, Cd, Hg, Pb) concentrations of three edible seaweed species. </p>
              
              <a href="#" class="btn btn-primary mt-auto">Go somewhere</a>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
    <!-- Call to Action Well -->
</br>
</div>



<?php
include('footer.php');
?>