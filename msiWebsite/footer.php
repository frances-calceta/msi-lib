  <!-- Footer -->
  <footer class="footer py-5 bg-dark">
    <div class="container">
      <!-- <p class="m-0 text-center text-white">Copyright &copy; MSI Library 2019</p> -->
      <div class="row">
        <div class="col-lg-4"><p>
          Tel: 981-8500 Loc 2193 <br>
          Email: library@msi.upd.edu.ph<br>
          Webste. www.msi.upd.edu.ph/library</p>
        </div>
        <div class="col-lg-4"><p>
          The Marine Science Institute<br>
          College of Science<br>
          University of the Philippines Diliman Quezon City 1101</p>
        </div>
        <div class="col-lg-4"><p>
          Monday - Friday 8:00 a.m. – 6:00 p.m.</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
