<div class="col-md-3">
<div id="accordion">

  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <a href="" class="collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          <b>OPAC</b> <br>(Open Public Access Catalog)
        </a>
      </h5>
    </div>
    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-group list-group-flush">
          <li class="list-group-item"><a href="">MSI</a></li>
          <li class="list-group-item"><a href="">Diliman Libraries</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <a href="" class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <b>UP Subscriptions</b>
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-group list-group-flush">
          <li class="list-group-item"><a href="">Annual Reviews</a></li>
          <li class="list-group-item"><a href="">E-books</a></li>
          <li class="list-group-item"><a href="">EBSCO Host</a></li>          
          <li class="list-group-item"><a href="">Nature</a></li>
          <li class="list-group-item"><a href="">ProQuest</a></li>
          <li class="list-group-item"><a href="">Science Magazine</a></li>
          <li class="list-group-item"><a href="">Science Direct</a></li>
          <li class="list-group-item"><a href="">Scopus</a></li>
          <li class="list-group-item"><a href="">Springer Link</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <a href="" class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <b>Special Collections</b>
        </a>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-group list-group-flush">
          <li class="list-group-item"><a href="">MSI Publications</a></li>
          <li class="list-group-item"><a href="">Bolinao Researches</a></li>
          <li class="list-group-item"><a href="">West Philippine Sea</a></li
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h5 class="mb-0">
        <a href="" class="collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          <b>Related Links</b>
        </a>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-group list-group-flush">
          <li class="list-group-item"><a href="">MSI</a></li>
          <li class="list-group-item"><a href="">CS Library</a></li>
          <li class="list-group-item"><a href="">UP Main Library</a></li>
          <li class="list-group-item"><a href="">UP System</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFive">
      <h5 class="mb-0">
        <a href="" class="collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          <b>Other Resources</b>
        </a>
      </h5>
    </div>
    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-group list-group-flush">
          <li class="list-group-item"><a href="">Open Access</a></li>
          <li class="list-group-item"><a href="">IAMSLIC</a></li>
          <li class="list-group-item"><a href="">Marine Science Institutions</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingSix">
      <h5 class="mb-0">
        <a href="" class="collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
          <b>Books for Sale</b>
        </a>
      </h5>
    </div>
    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-group list-group-flush">
          <li class="list-group-item"><a href="">Books</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingSeven">
      <h5 class="mb-0">
        <a href="" class="collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
          <b>UPD Remote Access</b>
        </a>
      </h5>
    </div>
    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-group list-group-flush">
          <li class="list-group-item"><a href="">Remote Access</a></li>
        </ul>
      </div>
    </div>
  </div>

</div>
</div>